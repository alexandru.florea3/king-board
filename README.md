# King Board

King Board este o tabla de sah inteligenta, capabila sa detecteze mutarile jucatorilor. Detectia se face cu ajutorul magnetilor din piese si a senzorilor Hall situati sub tabla de sah. Datele primite de la senzori sunt procesate de un Arduino.

Tabla este insotita de o aplicatie mobila care comunica prin bluetooth cu aceasta, si care permite stocarea jocului sub forma de PGN. Aplicatia mai are suplimentar si functionalitate de cronometru de sah.

## Adresa repository-ului proiectului
https://gitlab.upt.ro/alexandru.florea3/king-board.git

## Vizualizarea aplicatiei
Documentul KingBoard.aia este specific aplicatiei mobile si reprezinta un proiect in mediul MIT App Inventor. 
Mediul poate fi accesat la adresa https://appinventor.mit.edu/explore/get-started.
Dupa accesare, fisierul KingBoard.aia poate fi importat si vizualizat in aces mediu.

## Instalarea aplicatiei
Aplicatia poate fi instalata descarcand fisierul KingBoard.apk si instalandu-l pe un device cu sistem de operare Android.

## Codul Arduino
Codul arduino este reprezentat de fisierul KingBoardArduino.ino. Acesta poate fi vizualizat si incarcat pe o placa arduino folosind ArduinoIDE.
