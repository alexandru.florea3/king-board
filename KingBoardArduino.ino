#include <SoftwareSerial.h>

#define S0 A0
#define S1 A1
#define S2 A2
#define S3 A3

#define RANK1 6
#define RANK2 7
#define RANK3 8
#define RANK4 9
#define RANK5 10
#define RANK6 11
#define RANK7 12
#define RANK8 13
#define RANKOFFSET 6 

#define DELAY 5

int white[8][8];    //stores the ACTUAL state of the WHITE pieces
int black[8][8];    //stores the ACTUAL state of the BLACK pieces

int prevWhite[8][8];  //stores the PREVIOUS state of the WHITE pieces
int prevBlack[8][8];  //same but for BLACK

int nrOfPhisicalMoves = 0;  //stores the number of time a player has made a move

//turns the board file from number to letter
String nrToLetter[8] = { "a","b","c","d","e","f","g","h" };

String board[8][8];   //the board with letters

int i, j, k, m = 0;   //used for iterations

void memorizeBoards();  //stores the state of current boards in prevWhite 
                        //                                  and prevBlack

void readWhite();       //reads the data from the sensors for the WHITE pieces
                        //and updates white[][] matrix 
void readBlack();       //same but for BLACK

String moveWhite();     //returns a PGN String with the move made by WHITE on the board
String moveBlack();     //same but for BLACK

SoftwareSerial hc06(2, 3);  //used for communicating with the HC-06 Bluetooth sensor

/////////////////////////
///SETUP
/////////////////////////
void setup() {

    Serial.begin(9600);
    hc06.begin(9600);

    //pins corelation
    pinMode(S0, OUTPUT);
    pinMode(S1, OUTPUT);
    pinMode(S2, OUTPUT);
    pinMode(S3, OUTPUT);

    pinMode(RANK1, INPUT);
    pinMode(RANK2, INPUT);
    pinMode(RANK3, INPUT);
    pinMode(RANK4, INPUT);
    pinMode(RANK5, INPUT);
    pinMode(RANK6, INPUT);
    pinMode(RANK7, INPUT);
    pinMode(RANK8, INPUT);

    //pins initial values
    digitalWrite(S0, LOW);
    digitalWrite(S1, LOW);
    digitalWrite(S2, LOW);
    digitalWrite(S3, LOW);

    //initialize board matrixes
    for (i = 0; i <= 7; i++)
        for (j = 0; j <= 7; j++)
        {
            white[i][j] = 0; black[i][j] = 0; 
            prevWhite[i][j] = 0; prevBlack[i][j] = 0;
        }

    //initialize the board bz placing each piece in it's place
    board[0][0] = "R"; board[1][0] = "P";
    board[0][1] = "N"; board[1][1] = "P";
    board[0][2] = "B"; board[1][2] = "P";
    board[0][3] = "Q"; board[1][3] = "P";
    board[0][4] = "K"; board[1][4] = "P";
    board[0][5] = "B"; board[1][5] = "P";
    board[0][6] = "N"; board[1][6] = "P";
    board[0][7] = "R"; board[1][7] = "P";

    board[7][0] = "R"; board[6][0] = "P";
    board[7][1] = "N"; board[6][1] = "P";
    board[7][2] = "B"; board[6][2] = "P";
    board[7][3] = "Q"; board[6][3] = "P";
    board[7][4] = "K"; board[6][4] = "P";
    board[7][5] = "B"; board[6][5] = "P";
    board[7][6] = "N"; board[6][6] = "P";
    board[7][7] = "R"; board[6][7] = "P";

    //clear the squares where pieces shouldn't be placed
    for (i = 2; i <= 5; i++)
        for (j = 0; j <= 7; j++)
            board[i][j] = " ";
    
    readWhite();          //reads the initial state of the white pieces
    readBlack();          //and of the black ones
    
    memorizeBoards();     //saves the boards. In the beginning,
                          //both whit/black and prevWhite/Black are the same
   
    nrOfPhisicalMoves=0;  //no moves made yet
    
    Serial.print("Log: Board Initialized\n");  //used for testing
}

/////////////////////////
///LOOP
/////////////////////////
void loop() {
    if (hc06.available() > 0)
    {
        int request = 0;
        request = hc06.read();
        Serial.print(request);

        if(request == 'R')          //refresh board before starting game
          setup();
          
        if (request == 'W' || request == 'B')
        {
            memorizeBoards();

            readWhite();
            readBlack();

            if (nrOfPhisicalMoves % 2 == 0)
                {
                  String _move = moveWhite();
                  //Serial.print(_move);
                  hc06.print(_move);
                  nrOfPhisicalMoves++;
                } 
            else
                {
                  String _move = moveBlack();
                  //Serial.print(_move);
                  hc06.print(_move);
                  nrOfPhisicalMoves++;
                }
          }
      }
}

/////////////////////////
///FUNCTIONS
/////////////////////////

String moveWhite()
{
    String movedPiece = " ";
    String wasCaptureMade = "";
    int flag = 0;
    int to_i, to_j, from_i, from_j;
    
    //if white short castle (both king and right h1 rook are moved)
    if(prevWhite[0][4]==0 && prevWhite[0][7]==0 && white[0][4]==1 && white[0][7]==1 )
    {
      board[0][4] = " "; board[0][7] = " ";    //clear the old squaares
      board[0][5] = "R"; board[0][6] = "K";    //set the king and rook 
                                               //to the new squares(a6 and a7)
      return String((nrOfPhisicalMoves / 2) + 1) + ". O-O";
    }
    //if white long castle (both king and right a1 rook are moved)
    else if(prevWhite[0][0]==0 && prevWhite[0][4]==0 && white[0][0]==1 && white[0][4]==1 ) 
    {
      board[0][0] = " "; board[0][4] = " ";    //clear the old squaares
      board[0][3] = "R"; board[0][2] = "K";    //set the king and rook 
                                               //to the new squares(a3 and a4)
      return String((nrOfPhisicalMoves / 2) + 1) + ". O-O-O";
    }
    else
    {
    for (i = 0; i <= 7 && flag < 2; i++)
        for (j = 0; j <= 7 && flag < 2; j++)
        {
            if (prevWhite[i][j] != white[i][j])
            {
                if (prevWhite[i][j] == 1)
                {
                    to_i = i;                  //store the destination's coords
                    to_j = j;

                    if (board[i][j] != " ")    //checks if a piece was situated where the crrent piece will move
                        wasCaptureMade = "x";  //if yes, that means we have a capture

                    flag++;
                }
                if (prevWhite[i][j] == 0)       
                {
                    from_i = i;                 //store the destination's coords
                    from_j = j;
                    movedPiece = board[i][j];   //store the lifted piece
                    board[i][j] = " ";          //remove the piece from the board
                    flag++;
                }
            }
        }
    //if both a destination and a origin were found
    if (flag == 2)
        //move the piece to the new location
        board[to_i][to_j] = movedPiece;
        
    //if Pawn reaches the last rank, promote to Queen
    if(movedPiece=="P" && to_i==7) 
      {
         board[to_i][to_j] = "Q";
        return String((nrOfPhisicalMoves / 2) + 1) + ". " + movedPiece + nrToLetter[from_j] + String(from_i + 1) + wasCaptureMade + nrToLetter[to_j] + String(to_i + 1) + "=Q";
      }
    //if it's just a simple move
    return String((nrOfPhisicalMoves / 2) + 1) + ". " + movedPiece + nrToLetter[from_j] + String(from_i + 1) + wasCaptureMade + nrToLetter[to_j] + String(to_i + 1);
    }
}

String moveBlack()
{
    String movedPiece = " ";
    String wasCaptureMade = "";
    int flag = 0;
    int to_i, to_j, from_i, from_j;


    if(prevBlack[7][4]==0 && prevBlack[7][7]==0 && black[7][4]==1 && black[7][7]==1 ) //if white short castle (both king and right h1 rook are moved)
    {
      board[7][4] = " "; board[7][7] = " ";    //clear the old squaares
      board[7][5] = "R"; board[7][6] = "K";    //set the king and rook tot the new squares(a6 and a7)
      return " O-O";
    }
    else if(prevBlack[7][0]==0 && prevBlack[7][4]==0 && black[7][0]==1 && black[7][4]==1 ) //if white long castle (both king and right h1 rook are moved)
    {
      board[7][0] = " "; board[7][4] = " ";    //clear the old squaares
      board[7][3] = "R"; board[7][2] = "K";    //set the king and rook tot the new squares(a3 and a4)
      return " O-O-O";
    }
    else{

    for (i = 0; i <= 7 && flag < 2; i++)
        for (j = 0; j <= 7 && flag < 2; j++)
        {
            if (prevBlack[i][j] != black[i][j])
            {
                if (prevBlack[i][j] == 1)
                {
                    to_i = i;                  //store the destination's coords
                    to_j = j;

                    if (board[i][j] != " ")    //checks if a piece was situated where the crrent piece will move
                        wasCaptureMade = "x";  //if yes, that means we have a capture

                    flag++;
                }
                if (prevBlack[i][j] == 0)       //could be an "else" statement
                {
                    from_i = i;                 //store the destination's coords
                    from_j = j;
                    movedPiece = board[i][j];   //store the lifted piece
                    board[i][j] = " ";          //remove the piece from the board
                    flag++;
                }
            }
        }
    if (flag == 2)
        board[to_i][to_j] = movedPiece;

    if(movedPiece=="P" && to_i==0)              //if Pawn reaches the last rank, promote to Queen
      {
         board[to_i][to_j] = "Q";
        return " " + movedPiece + nrToLetter[from_j] + String(from_i + 1) + wasCaptureMade + nrToLetter[to_j] + String(to_i + 1) + "=Q ";
      }
    return " " + movedPiece + nrToLetter[from_j] + String(from_i + 1) + wasCaptureMade + nrToLetter[to_j] + String(to_i + 1) + " ";
    }
}


void readWhite()
{
    digitalWrite(S0, LOW); //because white pieces are detected by even addresses
                           //and S0 represents binary the least significant bit
    int rankCount = 0;
    int fileCount = 0;

    for (i = 0; i <= 1; i++)
    {
        if (i == 0)digitalWrite(S3, LOW);
        else digitalWrite(S3, HIGH);

        for (j = 0; j <= 1; j++)
        {
            if (j == 0)digitalWrite(S2, LOW);
            else digitalWrite(S2, HIGH);

            for (k = 0; k <= 1; k++)
            {
                if (k == 0)digitalWrite(S1, LOW);
                else digitalWrite(S1, HIGH);

                //read "Z" values and store in matrix
                for (rankCount = RANK8; rankCount >= RANK1; rankCount--)
                {
                    int x = digitalRead(rankCount);
                    white[rankCount - RANKOFFSET][7 - fileCount] = x;
                    //Serial.print(x);
                }

                //go to the next file
                fileCount++;
            }
        }
    }
}

void readBlack()
{
    digitalWrite(S0, HIGH);
    int rankCount = 0;
    int fileCount = 0;

    for (i = 0; i <= 1; i++)
    {
        if (i == 0)digitalWrite(S3, LOW);
        else digitalWrite(S3, HIGH);

        for (j = 0; j <= 1; j++)
        {
            if (j == 0)digitalWrite(S2, LOW);
            else digitalWrite(S2, HIGH);

            for (k = 0; k <= 1; k++)
            {
                if (k == 0)digitalWrite(S1, LOW);
                else digitalWrite(S1, HIGH);

                //read "Z" values and store in matrix
                for (rankCount = RANK8; rankCount >= RANK1; rankCount--)
                {
                    int x = digitalRead(rankCount);
                    black[rankCount - RANKOFFSET][7 - fileCount] = x;
                    //Serial.print(x);
                }

                //go to the next file
                fileCount++;
            }
        }
    }

}

void memorizeBoards()
{
    for (i = 0; i <= 7; i++)
        for (j = 0; j <= 7; j++)
        {
            prevWhite[i][j] = white[i][j];
            prevBlack[i][j] = black[i][j];
        }
}
